from django.db import models
from django.utils import timezone
from local import constants


class Local(models.Model):
    ''' Classe que representa a localização (endereço) '''
    logradouro = models.CharField('endereço', max_length=255, null=True, blank=True)
    numero = models.CharField('número', max_length=16, null=True, blank=True)
    complemento = models.CharField('complemento', max_length=100, null=True, blank=True)
    bairro = models.CharField('bairro', max_length=255, null=True, blank=True)
    cidade = models.CharField('cidade', max_length=255, null=True, blank=True)
    estado = models.CharField('estado', max_length=2, null=True, blank=True,
                                choices=constants.UF_SIGLA)
    cep = models.CharField('cep', max_length=9, null=True, blank=True)
    geocode = models.CharField('geocode', max_length=32, null=True, blank=True)
    criado_em = models.DateTimeField('data de criação', editable=False)
    editado_em = models.DateTimeField('data de atualização', null=True, blank=True)

    class Meta:
        ordering = ('logradouro',)
        verbose_name = 'local'
        verbose_name_plural = 'locais'

    def __str__(self):
        return self.logradouro + ', ' + self.numero

    def save(self, *args, **kwargs):
        if not self.criado_em:
            self.criado_em = timezone.now()
        self.editado_em = timezone.now()
        return super(Local, self).save(*args, **kwargs)

    def get_endereco_completo(self):
        endereco = self.logradouro
        endereco += ", {}".format(self.numero)
        endereco += " - {}".format(self.complemento)
        endereco += " - {}".format(self.bairro)
        endereco += " - {}".format(self.cidade)
        endereco += " - {}".format(self.estado)
        endereco += " - CEP {}".format(self.cep)
        return endereco

    def get_endereco(self):
        endereco = self.logradouro
        endereco += ", {}".format(self.numero)
        endereco += " - {}".format(self.bairro)
        endereco += " - {}".format(self.cidade)
        endereco += " - {}".format(self.estado)
        return endereco
