from django.test import TestCase
from local import constants
from local.models import Local


class LocalTestCase(TestCase):
    ''' Teste unitário da classe Local '''

    def setUp(self):
        Local.objects.create(
            logradouro='Rua Prof. Celso Ferraz de Camargo',
            numero='350',
            bairro='Cidade Universitária',
            complemento='Bloco B',
            cidade='Campinas',
            estado=constants.SP,
            cep='13083-200',
            geocode='-22.803997, -47.064340'
        )

    def test_get(self):
        celso_ferraz = Local.objects.get(
                logradouro='Rua Prof. Celso Ferraz de Camargo',
                numero='350')
        self.assertIsInstance(celso_ferraz, Local)

    def test_get_endereco_completo(self):
        celso_ferraz = Local.objects.get(
                logradouro='Rua Prof. Celso Ferraz de Camargo',
                numero='350')
        self.assertEqual(celso_ferraz.get_endereco_completo(),
                'Rua Prof. Celso Ferraz de Camargo, 350 - Bloco B - Cidade Universitária - Campinas - SP - CEP 13083-200')

    def test_get_endereco(self):
        celso_ferraz = Local.objects.get(
                logradouro='Rua Prof. Celso Ferraz de Camargo',
                numero='350')
        self.assertEqual(celso_ferraz.get_endereco(),
                'Rua Prof. Celso Ferraz de Camargo, 350 - Cidade Universitária - Campinas - SP')
